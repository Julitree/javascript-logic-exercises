//Given an array of integers your solution should find the smallest integer.

var findSmallestInt = (args) => {
    return Math.min.apply(Math, args)
}


findSmallestInt([78, 56, 232, 12, 8]) //8
