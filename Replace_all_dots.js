
//The code provided is supposed replace all the dots . in the specified String str with dashes -

const replaceDots = function(str) {
    let x = '';
    for (let i = 0; i < str.length; i++) {
        if (str[i] == '.') {
            x += '-';
        } else {
            x += str[i];
        }
    }

    return x
}

console.log(replaceDots("one.two.three"))
