//You will be given an array a and a value x. All you need to do is check whether the provided array contains the value.
//Array can contain numbers or strings. X can be either.
//Return true if the array contains the value, false if not.

function check(a, x) {
    var newArray = a.filter(element => element === x);
    let isInArray = newArray.length == 0 ? false : true;
    return isInArray
}

check([66, 101], 66);
check([80, 117, 115, 104, 45, 85, 112, 115], 45);